# Test entretien 

L’entreprise MA-SAUVEGARDE souhaite développer un extranet permettant la gestion de ses clients.
Pour cela, vous avez la charge de développer deux parties :
L’enregistrement de nouveaux clients
La connexion de ces derniers à leur interface de gestion

# Information d'Instalation

- Ouvrir phpmyadmin connection en localhost  username : "root" /  password : ""
- Dans phpmyadmin importer le fichier masauvegarde.sql présent dans ce repository
- Faite un clone de ce repository dans le dossier www de wamp.
- Pour tester aller sur http://localhost/test-entretien/public/



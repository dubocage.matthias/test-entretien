<?php
require 'Controller.php';

class AuthController extends Controller{

    public function login(){
        if($this->logged()){
            return $this->view('auth.welcome');
        }
        return $this->view('auth.login');
    }

    public function register(){
        if($this->logged()){
            return $this->view('auth.welcome');
        }
        return $this->view('auth.register');
    }

    public function saveRegister(){
        if(isset($_POST) && !empty($_POST['username']) && !empty($_POST['password'])
            && !empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['age']))
        {
            extract($_POST);
            $lastname = $_POST['lastname'];
            $firstname = $_POST['firstname'];
            $age = $_POST['age'];
            $username = $_POST['username'];
            $password = $_POST['password'];

            $stmt = $this->db->getPDO()->prepare('SELECT * FROM user WHERE username = ?');
            $stmt->execute([$username]);
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            if($result == null && $age >= 1 && $age < 200 && preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{6,}$#', $password)){
                $password =  hash('ripemd160',$_POST['password']);
                $stmt = $this->db->getPDO()->prepare('INSERT INTO user (firstname,lastname,username,age,password) VALUES (:firstname,:lastname,:username,:age,:password)');
                $stmt->bindParam(':age', $age, PDO::PARAM_INT);
                $stmt->bindParam(':firstname', $firstname,);
                $stmt->bindParam(':username', $username, );
                $stmt->bindParam(':lastname', $lastname,);
                $stmt->bindParam(':password', $password, );
                $stmt->execute();
                
                return $this->view('auth.login');
            }
        }
        return $this->view('auth.register');
    }

    public function connection(){
        if(isset($_POST) && !empty($_POST['username']) && !empty($_POST['password'])){
            extract($_POST);
            $username = $_POST['username'];
            $password =  hash('ripemd160',$_POST['password']);
            $stmt = $this->db->getPDO()->prepare("SELECT * FROM user WHERE userName = '$username' AND password = '$password'");
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            if($result->username == $username && $result->password == $password){
                $_SESSION['Auth'] = array(
                    'username' => $username,
                    'password' => $password
                );
            }
            return $this->view('auth.welcome');
        }
        return $this->view('auth.login');
    }

    public function welcome(){
        if($this->logged()){
            return $this->view('auth.welcome');
        }
        return $this->view('auth.login');
    }

    public function logged(){
        if(isset($_SESSION['Auth']) && isset($_SESSION['Auth']['username']) && isset($_SESSION['Auth']['password'])){
            extract($_SESSION['Auth']);
            $username = $_SESSION['Auth']['username'];
            $password = $_SESSION['Auth']['password'];
            $stmt = $this->db->getPDO()->prepare("SELECT * FROM user WHERE userName = '$username' AND password = '$password'");
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            if($result->username == $username && $result->password == $password)
                return true;
        }
        
        return false;
    }

    public function logout(){
            $_SESSION = array();
            session_destroy();
            return $this->view('auth.login');
    }
}
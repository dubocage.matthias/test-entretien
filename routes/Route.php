<?php

require '../database/DBConnection.php';

class Route
{
    public $action;
    public $path;
    public $matches;

    public function __construct($path,$action)
    {
        $this->path = $path;
        $this->action = $action;
    }

    public function matches(string $url)
    {
        $path = preg_replace('#:([\w]+)#', '([^/]+)' , $this->path);
        $pathToMatch = "#^$path$#";

        if(preg_match($pathToMatch,$url,$matches)){
            $this->matches = $matches;
            return true;
        }

        return false;
    }

    public function execute()
    {
        $params = explode('@', $this->action);
        $controller = new $params[0](new DBConnection('masauvegarde','127.0.0.1','root',''));
        $method = $params[1];
        
        isset($this->matches[1]) ? $controller->$method($this->matches[1]) : $controller->$method();
    }
}
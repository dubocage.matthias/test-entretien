<h1>Login</h1>
<form action="login" method='post'>
    <div class="form-group">
        <label for="username">Identifiant</label>
        <input type="text" name="username" class="form-control" id="username" placeholder="Entrer votre identifiant" required>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password"  pattern=".{6,}"  placeholder="Entrer votre mot de passe" required>
    </div>
    <button type="submit" class="btn btn-primary">Valider</button>
</form>
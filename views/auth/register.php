<h1>Register</h1>
<form action="register" method='post'>
    <div class="form-group">
        <label for="lastname">Nom</label>
        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Enter votre nom" required>
    </div>
    <div class="form-group">
        <label for="firstname">Prenom</label>
        <input type="text"name="firstname" class="form-control" id="firstname" placeholder="Entrer votre prenom" required>
    </div>
    <div class="form-group">
        <label for="age">Age</label>
        <input type="number" name="age" min="1" max="200" class="form-control" id="age"  placeholder="Enter votre age" required>
    </div>
    <div class="form-group">
        <label for="username">Identifiant</label>
        <input type="text" name="username" class="form-control" id="username" placeholder="Entrer votre identifiant" required>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password"  pattern=".{6,}"  placeholder="Entrer votre mot de passe" required>
    </div>
    <button type="submit" class="btn btn-primary">Valider</button>
</form>
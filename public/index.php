<?php 

session_start();

require '../routes/Router.php';

define('VIEWS', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR );

$router = new Router( $_GET['url']);

$router->get('public/','AuthController@login');
$router->get('public/login','AuthController@login');
$router->post('public/login','AuthController@connection');
$router->get('public/register', 'AuthController@register');
$router->post('public/register' , 'AuthController@saveRegister');
$router->get('public/logout' , 'AuthController@logout');
$router->get('public/welcome', 'AuthController@welcome');

$router->run();